import React from "react";

// Creates UserContext object

const UserContext = React.createContext()

export const UserProvider = UserContext.Provider;

export default UserContext
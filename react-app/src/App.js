// when the dev wants to render multiple components inside a reuseable function such as App, Fragment component of react is used to enclosed these components to avoid errors in our frontend application; the pair of <> and </> can also be used
import { UserProvider } from './UserContext';
import { Fragment, useEffect, useState } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router,Routes,Route } from 'react-router-dom';
import Image from '../src/images/2.png'
// Web Components
import AppNavbar from './AppNavbar';

// Web Pages
import Home from "./pages/Home";
// import Courses from "./pages/Courses";
// import CourseView from './components/CourseView';
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import PageNotFound from "./pages/PageNotFound";
import Product from './pages/Product';
import ProductView from './components/ProductView'
import "./App.css";
import CartPage from './pages/CartPage';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear();
  }

  // useEffect(()=>{
  //   console.log(user)
  //   console.log(localStorage)
  // },[user])
  useEffect(() => {

    // console.log(user);
  
    fetch(`https://rhythm-express.herokuapp.com/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
  
      // Set the user states values with the user details upon successful login.
      if (typeof data._id !== "undefined") {
  
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
  
      // Else set the user states to the initial values
      } else {
  
        setUser({
          id: null,
          isAdmin: null
        });
  
      }
  
    })
  
    }, []);
  

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        {/* <Container fluid> */}
        <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path="/products/active" element={<Product />} />
          <Route path="/products/:productId" element={<ProductView />} />
          <Route path='/cart' element={<CartPage/>}/>
          <Route path='/register' element={<Register/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/logout' element={<Logout/>}/>
          <Route path='*' element={<PageNotFound/>} />
        </Routes>
        {/* <img src={Image} height={250} width={250} /> */}
        {/* </Container> */}
      </Router>
    </UserProvider>
    
  );
}

export default App;
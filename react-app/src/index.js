import React from 'react';
import ReactDOM from 'react-dom/client';
// import bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'
import 'antd/dist/antd.css'
import App from './App';
import { StoreProvider } from './Store';
import { HelmetProvider } from "react-helmet-async";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <StoreProvider>
      <HelmetProvider>
    <App />
    </HelmetProvider>
    </StoreProvider>
  </React.StrictMode>
);


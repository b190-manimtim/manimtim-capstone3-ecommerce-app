import { useState, Fragment, useContext, useEffect } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link, NavLink, useParams } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import UserContext from "./UserContext";
import Badge from 'react-bootstrap/Badge';
import Swal from 'sweetalert2'

// font awesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faInstagram,
  faTwitter,
  
} from "@fortawesome/free-brands-svg-icons";
import { faAt,faCartPlus } from "@fortawesome/free-solid-svg-icons";
import {
  solid,
  regular,
  brands,
  icon,
} from "@fortawesome/fontawesome-svg-core/import.macro"; // <-- import styles to be used
import { Dropdown } from "react-bootstrap";
import { Store } from "./Store";

export default function AppNavbar() {
  // localStorage.getItem is used to get a piece of information from the browser's local storage. Devs usually insert information in the local storage for specific purposes; one of commonly used purpose is for login
  const { user } = useContext(UserContext);
  const [query, setQuery] = useState("")
  const { state } = useContext(Store)
  const { cart } = state


  


  return (
    <section className="header">
      {/* <Navbar.Brand as={Link} to="/"> */}
      {/* <img src={logo}></img> */}
      <div className="rightNav">
        <Nav.Link as={NavLink} className="font" to="/" exact>
          Home
        </Nav.Link>
        <Nav.Link as={NavLink} className="font" to="/products/active" exact>
          {" "}
          Shop
        </Nav.Link>
        <Nav.Link as={NavLink} className="font" to="/products/active" exact>
          {" "}
          Accessories
        </Nav.Link>
        <Nav.Link as={NavLink} className="font" to="/products/active" exact>
          {" "}
          New
        </Nav.Link>
      </div>
      <Form className="d-flex">
        <Form.Control
          type="search"
          placeholder="Search"
          className="searchBar"
          aria-label="Search"
        />
        {/* <Link className="btn btn-primary" to={`/products/${_id}`}>
            Search
            </Link>

        <Button variant="primary" onClick={(e)=>search(productId)} block>search</Button> */}
        <Button variant="outline-success" className="btnSearch">Search</Button>
      </Form>
      {/* </Navbar.Brand> */}
      <Nav>
        <Dropdown alignRight>
          <Dropdown.Toggle>
          {/* <Nav.Link as={NavLink} className="font" to="/cart" exact> */}
          <FontAwesomeIcon icon={faCartPlus} className="cart" size="1x"/>

          {cart.cartItems.length > 0 && (
                <Badge pill bg="danger">
                  {cart.cartItems.reduce((acc, cur) => acc + cur.quantity, 0)}
                </Badge>
              )}

          {/* </Nav.Link> */}
          </Dropdown.Toggle>
          <Dropdown.Menu style={{minWidth: 370}}>
            <Link to={"/cart"}>View Cart</Link>
            {/* <span style={{padding: 10}}>Cart is Empty</span> */}
          </Dropdown.Menu>
          </Dropdown>
          </Nav>          
          
      <div>
        <ul className="navbar">
          <Navbar expand="lg">
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="ml-auto">
              
                {user.id !== null ? (
                  
                  <Nav.Link as={NavLink} className="font" to="/logout" exact>
                    {" "}
                    Logout
                  </Nav.Link>
                  
                ) : (
                  <Fragment>
                    <Nav.Link as={NavLink} className="font" to="/login" exact>
                      {" "}
                      Login
                    </Nav.Link>
                    <Nav.Link
                      as={NavLink}
                      className="font"
                      to="/register"
                      exact
                    >
                      {" "}
                      Register
                    </Nav.Link>
                    <div className="logoIcons">
                      <a href="https://www.facebook.com" target="_blank">
                        <FontAwesomeIcon
                          icon={faFacebook}
                          className="navIcons"
                          size="1x"
                        />
                      </a>

                      <a href="https://www.instagram.com" target="_blank">
                        <FontAwesomeIcon
                          icon={faInstagram}
                          className="navIcons"
                          size="1x"
                        />
                      </a>

                      <a href="https://www.twitter.com" target="_blank">
                        <FontAwesomeIcon
                          icon={faTwitter}
                          className="navIcons"
                          size="1x"
                        />
                      </a>
                      <a href="https://www.gmail.com" target="_blank">
                        <FontAwesomeIcon icon={faAt} className="navIcons" size="1x"/>
                      </a>
                    </div>
                  </Fragment>
                )}
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </ul>
      </div>
    </section>
    // 			<Navbar bg="dark" expand="lg" variant='dark'>
    // 				{/* <Navbar.Brand as={Link} to='/'>Rhythm Express</Navbar.Brand> */}
    // 				<Navbar.Toggle aria-controls="basic-navbar-nav" />
    // 				<Navbar.Collapse id="basic-navbar-nav">
    // 					<Nav className="ml-auto">
    // 						{/* <Nav.Link as={NavLink} to='/' exact>Home</Nav.Link> */}
    // 						<>
    // 						<Nav.Link as={NavLink} to='/products/active' exact> Shop</Nav.Link>
    // 						<Nav.Link as={NavLink} to='/products/active' exact> Accessories <Badge count={1} bg="warning"></Badge></Nav.Link>
    // 						<Nav.Link as={NavLink} to='/products/active' exact> Sale</Nav.Link>
    // 						<Nav.Link as={NavLink} to='#' exact><FontAwesomeIcon icon={faFacebook} /></Nav.Link>

    //                         {(user.id !== null) ?
    //                         <Nav.Link as={NavLink} to='/logout' exact> Logout</Nav.Link>
    //                         :
    //                             <Fragment>
    //                                 <Nav.Link as={NavLink} to='/login' exact> Login</Nav.Link>
    //                                 <Nav.Link as={NavLink} to='/register' exact> Register</Nav.Link>
    //                             </Fragment>
    //                         }
    // </>
    // 					</Nav>
    // 				</Navbar.Collapse>
    // 			</Navbar>
  );
}

import ReactStars from "react-rating-stars-component";
import React from "react";
import { render } from "react-dom";
import ProductCard from "./ProductCard";

const ratingChanged = (props) => {
    const { rating, numReviews, caption } = props;
};

render(
<ReactStars
    count={5}
    onChange={ratingChanged}
    size={24}
    isHalf={true}
    emptyIcon={<i className="far fa-star"></i>}
    halfIcon={<i className="fa fa-star-half-alt"></i>}
    fullIcon={<i className="fa fa-star"></i>}
    activeColor="#ffd700"
/>,

document.querySelector("#productCard")
);
export default ratingChanged;
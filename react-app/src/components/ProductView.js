import React from 'react';

import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from 'sweetalert2'
import { useCart } from "react-use-cart";
import UserContext from "../UserContext";
import {CartState, Store} from "../Store"


export default function CourseView(props){
    const { user } = useContext(UserContext)
    const navigate = useNavigate()
    const [product, setProduct] = useState({});
    const { productId } = useParams()
    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [img, setImg] = useState("")
    const [price, setPrice] = useState(0)
    const [stock, setStock] = useState(0)
    

    useEffect(()=>{
        console.log(productId)

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(res => res.json())
        .then(data=>{
            console.log(data)

            setName(data.name)
            setDescription(data.description)
            setStock(data.stock)
            setImg(data.img)
            setPrice(data.price)
            
        })
    },[productId])

    const { state, dispatch: ctxDispatch } = useContext(Store);
    const { cart } = state;
    const addToCartHandler = () => {
    const existItem = cart.cartItems.find((x) => x._id === productId);
    const quantity = existItem ? existItem.quantity + 1 : 1;
    const productData = fetch(`http://localhost:4000/products/${productId}`)
        .then((res) => res.json())
        .then((data) => {
        if (data.stock < quantity) {
            window.alert("Product is out of stock");
            return;
        }
        });
    ctxDispatch({
        type: "CART_ADD_ITEM",
        payload: { ...product, quantity },
    });
    navigate("/cart");
    };


    return(
        <Container className="mt-5">
            <Row>
                <Col lg={{span: 6, offset:3}}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            
                            <Card.Img src={img}></Card.Img>
                                
                            

                            <Card.Subtitle>Description</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>

                            <Card.Subtitle>Price</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            

                            {(user.id !== null) ?
                            <Button variant="primary" onClick={addToCartHandler} block>Add To Cart</Button>
                            :
                            <Link className="btn btn-danger btn-block" to="/login">Add to cart</Link> 
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )


}

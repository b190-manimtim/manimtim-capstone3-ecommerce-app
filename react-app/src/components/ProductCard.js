import { Row, Col, Card, Button } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import {
    MDBCard,
    MDBCardImage,
    MDBCardBody,
    MDBCardTitle,
    MDBCardText,
    MDBRow,
    MDBCol,
    MDBCardSubTitle
} from 'mdb-react-ui-kit';
import styled from "styled-components";
import { Store } from "../Store";
// import Rating from "./Rating"


// we can try to destructure the props object recieved from the parent component(Courses.js) so that the main object from the mock database will be accessed
export default function ProductCard({ productProp }) {
  //console.log(props);
  //console.log(typeof props);

  // destructure the courseProp object for more code readability and ease of coding for the part of the dev (doesn't have to access multiple objects before accessing the needed properties)
    const { name, description, price, img, _id, newRating } = productProp;


    const { state, dispatch: ctxDispatch } = useContext(Store);
            const {
        cart: { cartItems },
    } = state;
    
    const addToCartHandler = async (item) => {
        const existItem = cartItems.find((x) => x._id === item._id);
        const quantity = existItem ? existItem.quantity + 1 : 1;
        const productData = fetch(
        `${process.env.REACT_APP_API_URL}
        /products/${productProp._id}`
        )
        .then((res) => res.json())
        .then((data) => {
            if (data.stock < quantity) {
            window.alert("Product is out of stock");
            return;
            }
        });
        ctxDispatch({
        type: "CART_ADD_ITEM",
        payload: { ...item, quantity },
        });
    };
    
// height:'350px', width:'310px'
// <MDBRow className='row-cols-1 row-cols-md-1 g-4'>

return (
    
    <MDBRow>
    <MDBCol>
        <MDBCard className="productCard" >
        <MDBCardImage
            className="img-fluid" src={img} waves style={{borderRadius:'10px',  height:'350px', width:'320px'}}
            
        />
        <MDBCardBody>
        <MDBCardTitle>{name}</MDBCardTitle>
        <MDBCardSubTitle>Description</MDBCardSubTitle>
        <MDBCardText>{description}</MDBCardText>
        <MDBCardSubTitle>Price: {price}</MDBCardSubTitle>
        {/* <Rating rating={newRating}></Rating> */}
        <Link className="btn btn-primary" to={`/products/${_id}`}>
            Details
            </Link>

            <Button className="btnAdd" onClick={() => addToCartHandler(productProp)}>
            Add to Cart
        </Button>

        
        {/* handleClick={handleClick} */}
        </MDBCardBody>
        </MDBCard>
    </MDBCol>
    </MDBRow>
);
}
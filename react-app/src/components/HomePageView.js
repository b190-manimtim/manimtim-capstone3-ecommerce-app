import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "bootstrap/dist/css/bootstrap.min.css";
import image from "../images/3.png";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";

function HomePage() {
  return (
    // <img src={image} className="bgImage" />
    <section className="hero">
      <Row>
        <Col>
          <h1 className="readyH1">READY TO PLAY?</h1>
          <Link to="/products/active">
            <Button variant="light" className="rounded-pill btnViewShop">
              View Shop
            </Button>
          </Link>
          {/* <Link class="buy" to={destination}>
            {label}
          </Link> */}
        </Col>
      </Row>
    </section>
  );
}

export default HomePage;

import { Fragment, useEffect, useState, useContext } from "react";
//import ProductCard from "../components/ProductCard";
import AdminView from "../components/AdminView";
import UserView from "../components/UserView";
import UserContext from "../UserContext";
import ProductCard from "../components/ProductCard";

export default function Product() {

    const { user } = useContext(UserContext);

    const [product, setProduct] = useState([]);

    const fetchData = () => {
    fetch(`https://rhythm-express.herokuapp.com/products/`)
    .then((res) => res.json())
    .then((data) => {
        setProduct(data);
    });
};

useEffect(() => {
    fetchData();
}, []);

return (
    <Fragment>
    {user.isAdmin === true ? (
        <AdminView productData={product} fetchData={fetchData} />
    ) : (
        <UserView productData={product}/>
    )}
    </Fragment>
);
}
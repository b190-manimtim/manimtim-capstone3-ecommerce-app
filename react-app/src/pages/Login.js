import {Form, Button} from 'react-bootstrap'
import { useState, useEffect, useContext } from "react";
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Login(){
    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    const retrieveUserDetails = (token) => {
        fetch(`https://rhythm-express.herokuapp.com/users/profile`,{
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }    

    useEffect(() => {
        if((email !== '' && password !== '')
    ){
        setIsActive(true)
    }else{
        setIsActive(false)
    }

    }, [email, password]);

    function loginUser(e){
        e.preventDefault()

        // fetch method will communicate with our backend application providing it with a stringified JSON coming from the body
        fetch(`https://rhythm-express.herokuapp.com/users/login`,{
            method: 'POST',
            headers:{
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (typeof data.access !== 'undefined'){
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access)

                Swal.fire({
                    title:"Login Successful!",
                    icon: "success",
                    text: "Welcome to Rhythm Express!"
                })
            } else{
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Check your login credentials and try again"
                })
            }
        })

        // clear input fields
        

        setEmail('')
        setPassword('')

    }

    return (
        (user.id !== null)?
            <Navigate to='/products/active' />
            :
        <div className='main'>
            <div className='sub-main'>
        <Form onSubmit={(e)=> loginUser(e)}>
            <h1 class="text-center">Login</h1>
        <Form.Group  controlId="userEmail">
            <Form.Label className='email'>Email address</Form.Label>
            <Form.Control
                className='emailInput'
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={e => setEmail(e.target.value)}
                required/>
        </Form.Group>
    
        <Form.Group controlId="password">
            <Form.Label className='password'>Password</Form.Label>
            <Form.Control
            className='passwordInput'
            type="password"
            placeholder="Password"
            value={password}
            onChange={e => setPassword(e.target.value)}
            required/>
        </Form.Group>

        {isActive ? (
        <Button variant="success" type="submit" className="submitBtn" onClick={loginUser}>
            Submit
        </Button> 
        ) : (
            <Button variant="success" type="submit" className="submitBtn" onClick={loginUser} disabled>
            Submit
        </Button> 
        )}
        </Form>
            </div>
        </div>
    );
}
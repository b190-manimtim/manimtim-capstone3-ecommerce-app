import { Fragment } from 'react';
import HomePageView from '../components/HomePageView'

export default function Home(){
    return(
        <Fragment>
            <HomePageView />
        </Fragment>
    )
}
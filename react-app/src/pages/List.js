import { useState, useEffect, useContext } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from 'sweetalert2'

import UserContext from "../UserContext";

export default function CourseView(){
    
    const { user } = useContext(UserContext)

    const navigate = useNavigate()
    
    const { productId } = useParams()

    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [img, setImg] = useState("")
    const [price, setPrice] = useState(0)

    const enroll = (courseId) => {
        fetch(`https://rhythm-express.herokuapp.com/users/enroll`,{
            method: "POST",
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true){
                Swal.fire({
                    title: "Successfully Enrolled",
                    icon: "success",
                    text: "You have successfully enrolled for this course"
                })
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                })
            }
        })
    }

    useEffect(()=>{
        console.log(productId)

        fetch(`https://rhythm-express.herokuapp.com/products/${productId}`)
        .then(res => res.json())
        .then(data=>{
            console.log(data)

            setName(data.name)
            setDescription(data.description)
            setImg(data.img)
            setPrice(data.price)
        })
    },[productId])

    return(
        <Container className="mt-5">
            <Row>
                <Col lg={{span: 6, offset:3}}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Img src={img}></Card.Img>
                            
                            <Card.Subtitle>Description</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>

                            <Card.Subtitle>Price</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            

                            {(user.id !== null) ?
                            <Button variant="primary" onClick={()=>enroll(productId)} block>Enroll</Button>
                            :
                            <Link className="btn btn-danger btn-block" to="/login">Add to cart</Link> 
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )


}

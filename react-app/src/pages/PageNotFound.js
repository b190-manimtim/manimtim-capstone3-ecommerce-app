
import React from 'react';
import { Link } from 'react-router-dom';

const PageNotFound= () =>{
    
        return <div>
            <h1>Page Not found</h1>
            
            Go back to the <Link to="/">Home </Link>
            
        </div>;
    
}

export default PageNotFound;

import {Form, Button} from 'react-bootstrap'
import { useState, useEffect, useContext } from "react";
import  UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){
    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [firstname, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');
    const [mobileNum, setMobileNum] = useState('');
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();
    // to check if we have successfully implemented the 2-way binding
    console.log(email)
    console.log(password1)
    console.log(password2)

    useEffect(() => {
        if((email !== '' &&
        firstname !== '' &&
        lastname !== '' &&
        mobileNum !== '' && 
        password1 !== '' && 
        password2 !== '') 
        && (password1 === password2)
        && (mobileNum.length === 11)
    ){
        setIsActive(true)
    }else{
        setIsActive(false)
    }

    }, [email, password1,password2, firstname, lastname, mobileNum]);

function registerUser(e){
    e.preventDefault();
    fetch(`https://rhythm-express.herokuapp.com/users/checkEmail`,{
        method: 'POST',
        headers:{
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            email: email
        })
    })
    .then(res => res.json())
    .then(data => {
        if(data === false){
            fetch(`https://rhythm-express.herokuapp.com/users/register`,{
                method: 'POST',
                headers:{
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    firstName: firstname,
                    lastName: lastname,
                    email: email,
                    mobileNo: mobileNum,
                    password: password1
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                if(data.acess !== 'undefined'){
                    Swal.fire({
                        title: "Registration successful",
                        icon: "success",
                        text: "Welcome to Rhythm Express"
                    })
                    navigate('/login');
                } else {
                    Swal.fire({
                        title: "Duplicate email found!",
                        icon: "error",
                        text: "Please provide a different email"
                    })
                }
            })
        } else {
            Swal.fire({
                title: "Duplicate email found!",
                icon: "error",
                text: "Please provide a different email"
            })
        }
    })
}
    

    return (
        // (user.email !== null)?
        // <Navigate to='/courses' />
        //     :
        <div className='main'>
            <div className='sub-main2'>
        <Form onSubmit={(e)=> registerUser(e)}>
        <Form.Group  controlId="firstname">
        <h1 class="text-center">Register</h1>
            <Form.Label>First Name</Form.Label>
            <Form.Control
                type="text"
                placeholder="Enter first name"
                value={firstname}
                onChange={e => setFirstName(e.target.value)}
                required/>
        </Form.Group>

        <Form.Group  controlId="lastname">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
                type="text"
                placeholder="Enter last name"
                value={lastname}
                onChange={e => setLastName(e.target.value)}
                required/>
        </Form.Group>

        <Form.Group  controlId="mobileNum">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
                type="number"
                placeholder="Enter mobile number"
                value={mobileNum}
                onChange={e => setMobileNum(e.target.value)}
                required/>
        </Form.Group>

        <Form.Group  controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={e => setEmail(e.target.value)}
                required/>
            <Form.Text className="text-muted">
            We'll never share your email with anyone else.
            </Form.Text>
        </Form.Group>
    
        <Form.Group controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
            type="password"
            placeholder="Password"
            value={password1}
            onChange={e => setPassword1(e.target.value)}
            required/>
        </Form.Group>

        <Form.Group controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
            type="password"
            placeholder="Password"
            value={password2}
            onChange={e => setPassword2(e.target.value)}
            required/>
        </Form.Group>
        {isActive ? (
        <Button variant="primary" type="submit" className="submitBtn2" onClick={registerUser}>
            Submit
        </Button> 
        ) : (
            <Button variant="primary" type="submit" className="submitBtn2" onClick={registerUser} disabled>
            Submit
        </Button> 
        )}
        </Form>
            </div>
        </div>
    );
}